﻿namespace LCEDA_OBJ_Transform
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.local_folder_textBox = new System.Windows.Forms.TextBox();
            this.preview_textBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // local_folder_textBox
            // 
            this.local_folder_textBox.Location = new System.Drawing.Point(12, 12);
            this.local_folder_textBox.Name = "local_folder_textBox";
            this.local_folder_textBox.ReadOnly = true;
            this.local_folder_textBox.Size = new System.Drawing.Size(388, 21);
            this.local_folder_textBox.TabIndex = 17;
            // 
            // preview_textBox
            // 
            this.preview_textBox.Location = new System.Drawing.Point(12, 39);
            this.preview_textBox.Multiline = true;
            this.preview_textBox.Name = "preview_textBox";
            this.preview_textBox.ReadOnly = true;
            this.preview_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.preview_textBox.Size = new System.Drawing.Size(560, 410);
            this.preview_textBox.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Image = global::LCEDA_OBJ_Transform.Properties.Resources.shengchengshili;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(492, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 24);
            this.button1.TabIndex = 20;
            this.button1.Text = "转换文件";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button16
            // 
            this.button16.Image = global::LCEDA_OBJ_Transform.Properties.Resources.dakaiwenjianjia;
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(406, 10);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(80, 24);
            this.button16.TabIndex = 18;
            this.button16.Text = "打开文件";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.preview_textBox);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.local_folder_textBox);
            this.Name = "MainForm";
            this.Text = "OBJ文件结构转换 ——适用于LCEDA导出的obj文件 Powered by 矛盾聚合体";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TextBox local_folder_textBox;
        private System.Windows.Forms.TextBox preview_textBox;
        private System.Windows.Forms.Button button1;
    }
}

