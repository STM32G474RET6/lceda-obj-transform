﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LCEDA_OBJ_Transform
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private string input = "";

        private void button16_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog
            {
                Multiselect = true,
                Title = "请选择文件",
                Filter = "obj文件|*.obj|所有文件(*.*)|*.*"
            };
            if(fileDialog.ShowDialog() != DialogResult.OK) { return; }
            var filePath = fileDialog.FileName;
            if(Path.GetExtension(filePath).ToUpper() != ".OBJ")
            {
                MessageBox.Show("文件格式错误！\r\n请选择.obj文件...", "选择文件提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                local_folder_textBox.Text = "";
            }
            else
            {
                // 提示
                Thread thread = new Thread(() =>
                {
                    MessageBox.Show("正在打开文件，可能需要几分钟的时间，请耐心等候", "提示");
                });
                thread.Start();
                local_folder_textBox.Text = filePath; //.Substring(0, filePath.LastIndexOf('\\'));
                string path = filePath;       //获得路径
                var fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                var sr = new StreamReader(fs, Encoding.GetEncoding("utf-8"));
                input = sr.ReadToEnd();
                preview_textBox.Text = input;
                fs.Close();
                sr.Close();
                // 提示
                MessageBox.Show("文件打开完毕", "提示");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private class Mtl
        {
            public int Type = 0;
            public string mtlStr = "";
            public bool isRepeated = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // 提示
            Thread thread = new Thread(() =>
            {
                MessageBox.Show("正在转换文件，可能需要几分钟的时间，请耐心等候", "提示");
            });
            thread.Start();
            if(input == "") { return ; }
            // 提取材质
            List<Mtl> mtls = new List<Mtl>();
            var Pattern = @"Ka.+\nKd.+\nKs.+\n";
            var mtlsRegex = Regex.Matches(input, Pattern);
            for(var i = 0; i < mtlsRegex.Count; i++)
            {
                mtls.Add(new Mtl()
                {
                    Type = i,
                    mtlStr = mtlsRegex[i].Value
                });
                // -去掉重复材质，绑定相同材质
                for(int j = 0; j < mtls.Count - 1; j++)
                {
                    if(Regex.IsMatch(mtls[mtls.Count - 1].mtlStr, mtls[j].mtlStr) == true)
                    {
                        mtls[mtls.Count - 1].isRepeated = true;
                        mtls[mtls.Count - 1].Type = mtls[j].Type;
                        break;
                    }
                }
            }
            // 材质文件
            var mtlfile = "";
            foreach(var mtl in mtls)
            {
                if(mtl.isRepeated == true) { continue; }
                mtlfile += "newmtl mtl" + mtl.Type + "\n";
                mtlfile += mtl.mtlStr + "endmtl" + "\n";
            }
            // 模型文件
            var objFile = input;
            var Pattern1 = @"newmtl mtl.+\nKa.+\nKd.+\nKs.+\nendmtl\nusemtl .+\n";
            Regex regex = new Regex(Pattern1);
            int index = 0;
            while(Regex.IsMatch(objFile, Pattern1) == true)
            {
                string replacement = "\n" +
                                     "g " + index + "\n" +
                                     "usemtl mtl" + mtls[index].Type + "\n";
                index++;
                objFile = regex.Replace(objFile, replacement, 1);
            }
            objFile = Regex.Replace(objFile, "//", "");
            // -修改板厚
            var temp = @"(?<a>v -?[0-9]*[.]?[0-9]* -?[0-9]*[.]?[0-9]* )0.00";
            var objFileLeftArr = Regex.Split(objFile, "g 1\n")[0].Split('\n');
            var objFileRight = Regex.Split(objFile, "g 1\n")[1];
            objFile = "";
            foreach(var line in objFileLeftArr)
            {
                if(Regex.IsMatch(line, temp) == true)
                {
                    Regex regex1 = new Regex(temp);
                    // 10.00-3.80 = 6.20，6.20对应1.6mm板厚，换算得到
                    objFile += regex1.Matches(line)[0].Groups["a"].Value + "3.80" + "\n";
                }
                else
                {
                    objFile += line + "\n";
                }
            }
            // 组合剩下部分
            objFile += "g 1\n" + objFileRight;
            // 保存文件
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "(*.*)|*.*";
            // -将日期时间添加到文件名
            var fileNameRegex = @"^([a-zA-Z]:\\)([\s\.\-\w]+\\)*(?<fname>[\w]+.[\w]+)";
            saveFileDialog.FileName = Regex.Matches(local_folder_textBox.Text, fileNameRegex)[0].Groups["fname"] + "_" + DateTime.Now.ToString("yyyyMMddHHmm");
            if(saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // 创建mtl文件
                var streamWriter = new StreamWriter(saveFileDialog.FileName + ".mtl", true);
                streamWriter.Write(mtlfile);
                streamWriter.Close();
                // 创建obj文件
                streamWriter = new StreamWriter(saveFileDialog.FileName + ".obj", true);
                objFile = "mtllib " + saveFileDialog.FileName.Split('\\')[saveFileDialog.FileName.Split('\\').Length - 1]
                          + ".mtl\n" + objFile;
                streamWriter.Write(objFile);
                streamWriter.Close();
                // 展示文件内容
                preview_textBox.Text = "mtlfile: \n" + mtlfile
                                       + "\n\nobjFile: \n" + objFile;
                // 提示
                MessageBox.Show("文件转换完毕", "提示");
            }
        }
    }
}
